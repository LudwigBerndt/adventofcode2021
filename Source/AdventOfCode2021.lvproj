﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="20008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.Project.Description" Type="Str"></Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.acl" Type="Str">0800000008000000</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str"></Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.access" Type="Str"></Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.viscripting.showScriptingOperationsInContextHelp" Type="Bool">false</Property>
		<Property Name="server.viscripting.showScriptingOperationsInEditor" Type="Bool">false</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Common" Type="Folder">
			<Item Name="Input.vi" Type="VI" URL="../Input.vi"/>
			<Item Name="TimeDifference.vi" Type="VI" URL="../TimeDifference.vi"/>
			<Item Name="Input_raw.vi" Type="VI" URL="../Input_raw.vi"/>
		</Item>
		<Item Name="2020-12-01.lvlib" Type="Library" URL="../2020-12-01/2020-12-01.lvlib"/>
		<Item Name="2021-12-01.lvlib" Type="Library" URL="../2021-12-01/2021-12-01.lvlib"/>
		<Item Name="2021-12-02.lvlib" Type="Library" URL="../2021-12-02/2021-12-02.lvlib"/>
		<Item Name="2021-12-03.lvlib" Type="Library" URL="../2021-12-03/2021-12-03.lvlib"/>
		<Item Name="2021-12-04.lvlib" Type="Library" URL="../2021-12-04/2021-12-04.lvlib"/>
		<Item Name="2021-12-05.lvlib" Type="Library" URL="../2021-12-05/2021-12-05.lvlib"/>
		<Item Name="2021-12-06.lvlib" Type="Library" URL="../2021-12-06/2021-12-06.lvlib"/>
		<Item Name="2021-12-07.lvlib" Type="Library" URL="../2021-12-07/2021-12-07.lvlib"/>
		<Item Name="2021-12-08.lvlib" Type="Library" URL="../2021-12-08/2021-12-08.lvlib"/>
		<Item Name="2021-12-09.lvlib" Type="Library" URL="../2021-12-09/2021-12-09.lvlib"/>
		<Item Name="2021-12-10.lvlib" Type="Library" URL="../2021-12-10/2021-12-10.lvlib"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="High Resolution Relative Seconds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/High Resolution Relative Seconds.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
